<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class CreateUserEmail extends Mailable
{
    use Queueable, SerializesModels;

     public $user;
    public $password;
    public $link;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$password,$link)
    {
         $this->user = $user;
         $this->password=$password;
         $this->link=$link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.createuser')->subject('Calendar Intervi - User has been registered.');
    }
}
