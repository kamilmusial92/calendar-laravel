<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class CancelOrder extends Mailable
{
    use Queueable, SerializesModels;
   public $user;
    public $expired;
      public $link;
      public $orderid;
      public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$expired,$link,$orderid,$subject)
    {
         $this->user = $user;
         $this->expired=$expired;
          $this->link=$link;
           $this->orderid=$orderid;
           $this->subject=$subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       return $this->view('emails.cancelorder')->subject('Calendar Intervi - '.$this->subject.'.');
    }
}
