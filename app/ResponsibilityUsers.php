<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponsibilityUsers extends Model
{
     protected $fillable = [
        'user_id', 'text'
    ];
}

