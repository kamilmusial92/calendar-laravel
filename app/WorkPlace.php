<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkPlace extends Model
{
    protected $fillable = [
        'name', 'company_id'
    ];


	    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }

     public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }
    
       public function hasPermission($permission)
    {
        
                if($this->permissions()->where('name',$permission)->exists())
                {
                    return true;
                }


                
            
        
        
    }
}
