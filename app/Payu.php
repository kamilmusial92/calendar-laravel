<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use App\{CompanyPayment,CompanyInvoices, Subscription};
use App\Events\CreateOrderEvent;
use App\Events\CancelOrderEvent;
use Http;

class Payu extends Model
{

    private static $env = 'sandbox';


    private static $oauthClientId = '395407';

    private static $serviceUrl = '';

    private static $oauthClientSecret = '9437b66a9b6704a3190417945355ce96';



     public static function setEnvironment($domain = 'payu.com')
    {
    	$environment=self::$env;
        $environment = strtolower($environment);
        $page="https://secure";
        $domain = strtolower($domain);

   
        

        if ($environment == 'secure') {
            self::$serviceUrl = $page. '.' . $domain;
           
        } else if ($environment == 'sandbox') {
            self::$serviceUrl = $page.'.snd'. '.' . $domain;
           
        } 
    }

 
   public static function OAuthTrusted($user)
     {
        $client = new \GuzzleHttp\Client();
        Payu::setEnvironment();
        $path=self::$serviceUrl;
        $clientid=self::$oauthClientId;
        $clientsecret=self::$oauthClientSecret;
        $email=$user->email;
        $ext_customer_id=$user->id;

        $body = "grant_type=trusted_merchant&client_id=".$clientid."&client_secret=".$clientsecret."&email=".$email."&ext_customer_id=".$ext_customer_id;
                $response = $client->post('https://secure.snd.payu.com/pl/standard/user/oauth/authorize',[
             'headers' => [
                'Content-Type'=>'application/x-www-form-urlencoded'
            ],

              'body' => $body
                
            ]);

         return $access_token=json_decode($response->getBody()->getContents())->access_token;
     }


     public static function OAuth()
     {
        $client = new \GuzzleHttp\Client();
        
        Payu::setEnvironment();
        $path=self::$serviceUrl;
        $clientid=self::$oauthClientId;
        $clientsecret=self::$oauthClientSecret;

        $body = "grant_type=client_credentials&client_id=".$clientid."&client_secret=".$clientsecret;
                $response = $client->post('https://secure.snd.payu.com/pl/standard/user/oauth/authorize',[
             'headers' => [
                'Content-Type'=>'application/x-www-form-urlencoded'
            ],

              'body' => $body
                
            ]);

         return $access_token=json_decode($response->getBody()->getContents())->access_token;
     }

     public static function NewOrder($token,$subid,$price,$type,$user,$lang)
    {   





        $access_token=Payu::OAuth();


        $company=Company::with(['users'=>function($q) {
                 $q->whereHas('roles', function ($query) {
                    $query->where('name','Customer');
                 });
            },'access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.name','access.payment','slack'])->find($user->company_id);
                 
       

         
        $client = new \GuzzleHttp\Client();
        $clientid=self::$oauthClientId;
        $path=self::$serviceUrl;

            

                $email=$user->email;
                $firstname=substr($user->name, 0, strpos($user->name, " "));
                $lastname=substr($user->name, strpos($user->name, " "));
                $totalprice=floor(($price*100))/100;
                $totalprice=intval($price*100);

                if($type=='monthly')
                {
                     $enddate=date("Y-m-d H:i", strtotime(date("Y-m-d H:i"). ' + 31 day + 2hours'));
                }
                elseif($type=='yearly')
                {
                     $enddate=date("Y-m-d H:i", strtotime(date("Y-m-d H:i"). ' + 366 day + 2hours'));
                }

                $subscription=Subscription::findorFail($subid);

                $invoice=CompanyInvoices::create(['subscription_id'=>$subid,'company_id'=>$user->company_id,'access_id'=>2,'start'=>date("Y-m-d H:i"), 'end'=>$enddate,'status'=>'PENDING','renew_date'=>date("Y-m-d H:i",strtotime($enddate. ' - 2hours'))]);
                $payment=CompanyPayment::create(['invoice_id'=>$invoice->id,'payment_name'=>"Payu",'price'=>$totalprice/100,'type'=>$type,'lang'=>$lang,'renew'=>1]);

          $body= array(
                'notifyUrl'=> env('APP_CALENDAR').'/settings/company/'.$user->company_id.'/invoices/'.$invoice->id,
                    'customerIp'=> Payu::getUserIP(),
                    'merchantPosId'=> $clientid,
                     'recurring'=> "FIRST",
                    'description'=> 'Subscription '.$subscription->name.' - '.$type,
                    'currencyCode'=> 'PLN',
                    'totalAmount'=> $totalprice,  
                    'products'=> 
                    [
                     
                        [
                            'name'=> 'Subscription '.$type,
                            'unitPrice'=> $totalprice,
                            'quantity'=> '1'
                        ]
                        
                    ],
                     'buyer'=>array(
                      'email'=> $email,
                      'firstName'=> $firstname,
                      'lastName'=>$lastname,
                      'language'=> $lang
                    ),
                    'payMethods'=> array(
                        'payMethod'=>array(
                            'value'=> $token,
                            'type'=>  'CARD_TOKEN'
                          
                        )
                    )
            );

           $response = $client->post($path.'/api/v2_1/orders',[

             'headers' => [
                "Access-Control-Allow-Origin"=>"*",
                "Access-Control-Allow-Methods"=> "POST",
              
                'Content-Type'=>'application/json',
                "Authorization"=> "Bearer ".$access_token
            ],

              'body' => json_encode($body)
            
            ]);
           $res=json_decode($response->getBody()->getContents());

         

               if($res->status->statusCode=='SUCCESS')
               {
                $invoice->update(['status'=>'COMPLETED']);
                $payment->update(['token'=>$res->payMethods->payMethod->value,'orderId'=>$res->orderId]);
                $invoice=CompanyInvoices::findOrFail($invoice->id);
                $dateexpired=date("d-m-Y H:i",strtotime($invoice->renew_date));
                $link=env('APP_CALENDAR').'/settings/company/'.$user->company_id.'/invoices/'.$invoice->id;
                $orderid=$invoice->id;
                if(env('APP_ENV')=='production')
                event(new CreateOrderEvent($user,$dateexpired,$link,$orderid, 'New Order Subscription'));

                return response()->json([
                  'success' => true,
                  'message'=>'completed',
                 'data'=>$link
                ]);
               }
               elseif($res->status->statusCode=='WARNING_CONTINUE_3DS'){
                $payment->update(['token'=>$res->payMethods->payMethod->value,'orderId'=>$res->orderId]);
                 return response()->json([
                  'success' => true,
                 'data'=>$res->redirectUri
                ]);
               }
               else
               {
                $invoice->update(['status'=>'CANCELED']);
                 return response()->json([
                  'success' => true,
                 'data'=>"error",
                 'message'=>'Canceled'
                ]);
               }

   
                 

           

    }

    public static function getUserIP()
    {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                  $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                  $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }

        return $ip;
    }

     public static function checkOrder($id,$invoiceid)
     {
        $access_token=Payu::OAuth();
        $client = new \GuzzleHttp\Client();
        $clientid=self::$oauthClientId;
        $path=self::$serviceUrl;

         $response = $client->get($path.'/api/v2_1/orders/'.$id,[
             'headers' => [
                "Access-Control-Allow-Origin"=>"*",
                "Access-Control-Allow-Methods"=> "GET",
              
                'Content-Type'=>'application/json',
                "Authorization"=> "Bearer ".$access_token
            ]

              
            
            ]);
           $res=json_decode($response->getBody()->getContents());
           
           $order=CompanyInvoices::with(['payment','name','subscription'])->findorFail($invoiceid);

           if($res->orders[0]->status!=$order->status)
           {
            $order->update(['status'=>$res->orders[0]->status]);


           }
            if($res->orders[0]->status=='CANCELED')
                {
                     $order->payment[0]->update(['token'=>'','renew'=>0]);
                }

            return response()->json([
                  'success' => true,
                 'data'=>$order
                ]);
     }

     public static function cancelOrder($id,$user)
     {
         $payment=CompanyPayment::findorFail($id);
         $access_token=Payu::OAuthTrusted($user);

         $token=$payment->token;
         

         $payment->update(['token'=>'','renew'=>0]);
        
         $invoice=CompanyInvoices::with(['payment','name'])->findOrFail($payment->invoice_id);
        $dateexpired=date("d-m-Y H:i",strtotime($invoice->renew_date));
         $orderid=$invoice->id;
        $link=env('APP_CALENDAR').'/settings/company/'.$user->company_id.'/invoices/'.$invoice->id;
         if(env('APP_ENV')=='production')
         event(new CancelOrderEvent($user,$dateexpired,$link,$orderid, 'Subscription canceled'));

          

         $client = new \GuzzleHttp\Client();
           
            $path=self::$serviceUrl;


           /* $response = $client->delete($path.'/api/v2_1/tokens/'.$token,[
                 'headers' => [
                    "Access-Control-Allow-Origin"=>"*",
                    "Access-Control-Allow-Methods"=> "DELETE",
                  
                    'Content-Type'=>'application/json',
                    "Authorization"=> "Bearer ".$access_token
                ]

                
                
                ]);
            $res=json_decode($response->getBody()->getContents());*/

        return response()->json([
            'success' => true,
            'alert'=>true,
            'message'=>"subscriptionhasbeencanceled",
            'data'=>$invoice
            ]);
           

     }

     public static function orderRecurring($id,$user)
     {
        $payment=CompanyPayment::findorFail($id);
        $invoice=CompanyInvoices::findorFail($payment->invoice_id);
        $subscription=Subscription::findorFail($invoice->subscription_id);
        $now = date("Y-m-d H:i"); // or your date as well
        
        $datediff = strtotime($now) - strtotime($invoice->start);
        $datediff=round($datediff / (60 * 60 * 24));
       
        if(($datediff>=30 && $payment->type=='monthly') or ($datediff>=365 && $payment->type=='yearly'))
        {
       
            $access_token=Payu::OAuth();
             
            $client = new \GuzzleHttp\Client();
            $clientid=self::$oauthClientId;
            $path=self::$serviceUrl;
            $email=$user->email;
            $firstname=substr($user->name, 0, strpos($user->name, " "));
            $lastname=substr($user->name, strpos($user->name, " "));

            $totalprice=floor(($payment->price*100))/100;
            $totalprice=intval($payment->price*100);

                    if($payment->type=='monthly')
                    {
                         $enddate=date("Y-m-d H:i", strtotime(date("Y-m-d H:i"). ' + 31 day + 2hours'));
                    }
                    elseif($payment->type=='yearly')
                    {
                         $enddate=date("Y-m-d H:i", strtotime(date("Y-m-d H:i"). ' + 366 day + 2hours'));
                    }
                    

                    $invoice=CompanyInvoices::create(['subscription_id'=>$subscription->id,'company_id'=>$user->company_id,'access_id'=>2,'start'=>date("Y-m-d H:i"), 'end'=>$enddate,'status'=>'PENDING','renew_date'=>date("Y-m-d H:i",strtotime($enddate. ' - 2hours'))]);
                    $newpayment=CompanyPayment::create(['invoice_id'=>$invoice->id,'payment_name'=>"Payu",'price'=>$totalprice/100,'type'=>$payment->type,'lang'=>$payment->lang,'renew'=>1]);

                        $body= array(
                    'notifyUrl'=> env('APP_CALENDAR').'/settings/company/'.$user->company_id.'/invoices/'.$invoice->id,
                        'customerIp'=> Payu::getUserIP(),
                        'merchantPosId'=> $clientid,
                         'recurring'=> "STANDARD",
                        'description'=> 'Subscription '.$subscription->name.' - '.$payment->type,
                        'currencyCode'=> 'PLN',
                        'totalAmount'=> $totalprice,  
                        'products'=> 
                        [
                         
                            [
                                'name'=> 'Subscription '.$payment->type,
                                'unitPrice'=> $totalprice,
                                'quantity'=> '1'
                            ]
                            
                        ],
                         'buyer'=>array(
                          'email'=> $email,
                          'firstName'=> $firstname,
                          'lastName'=>$lastname,
                          'language'=> $payment->lang
                        ),
                        'payMethods'=> array(
                            'payMethod'=>array(
                                'value'=> $payment->token,
                                'type'=>  'CARD_TOKEN'
                              
                            )
                        )
                );

               $response = $client->post($path.'/api/v2_1/orders',[
                 'headers' => [
                    "Access-Control-Allow-Origin"=>"*",
                    "Access-Control-Allow-Methods"=> "POST",
                  
                    'Content-Type'=>'application/json',
                    "Authorization"=> "Bearer ".$access_token
                ],

                  'body' => json_encode($body)
                
                ]);
               $res=json_decode($response->getBody()->getContents());

                 

                   if($res->status->statusCode=='SUCCESS')
                   {
                    $invoice->update(['status'=>'COMPLETED']);
                    $newpayment->update(['token'=>$payment->token,'orderId'=>$res->orderId]);
                    $invoice=CompanyInvoices::findOrFail($invoice->id);
                     $dateexpired=date("d-m-Y H:i",strtotime($invoice->renew_date));
                     $orderid=$invoice->id;
                    $link=env('APP_CALENDAR').'/settings/company/'.$user->company_id.'/invoices/'.$invoice->id;
                    if(env('APP_ENV')=='production')
                    event(new CreateOrderEvent($user,$dateexpired,$link,$orderid, 'Subscription renewal'));

                    return response()->json([
                      'success' => true,
                      'message'=>'completed',
                     'data'=>$link
                    ]);
                   }
                   elseif($res->status->statusCode=='WARNING_CONTINUE_3DS'){
                    $newpayment->update(['token'=>$payment->token,'orderId'=>$res->orderId]);
                     return response()->json([
                      'success' => true,
                     'data'=>$res->redirectUri
                    ]);
                   }
                   else
                   {
                    $invoice->update(['status'=>'CANCELED']);
                     return response()->json([
                      'success' => true,
                     'data'=>"error",
                     'message'=>'Canceled'
                    ]);
                   }
        }
     }





}
