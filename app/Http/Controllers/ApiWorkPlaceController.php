<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\{WorkPlace, Company, User};
use Auth;


class ApiWorkPlaceController extends Controller
{
    public function authinfo()
    {
   
            
            $company=Company::find(Auth::user()->company_id);
            
            if(!$company)
            {
              return response()->json([
                'success' => true,
                'message' => 'accessdenied',
                  'data'=>[]
                 ]);
            }
            $workplaces=WorkPlace::where('company_id',$company->id)->orderBy('name')->get();

               if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $company->id==Auth::user()->company_id))
            {
           
              return response()->json([
                'success' => true,
                'data' => $workplaces,
                
              ]);
            }
            else{
              return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
        


    }

    public function info($id)
    {
    
        
            
            $company=Company::find($id);
            
            if(!$company)
            {
              return response()->json([
                'success' => true,
                'message' => 'accessdenied',
                  'data'=>[]
                 ]);
            }
          	$workplaces=WorkPlace::with('permissions')->where('company_id',$id)->orderBy('name')->get();

          	  if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {
           
              return response()->json([
                'success' => true,
                'data' => $workplaces,
                
              ]);
            }
            else{
              return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
       


    }

    public function edit(Request $request, $id)
    {
    
        
           
        
             $workplace=WorkPlace::findorFail($id);

            $company=Company::findorFail($workplace->company_id);

            if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {
              $value=$request['values'];
              $workplace->update(['name'=>$value['name']]);
            
           
              return response()->json([
                'success' => true,
                'data' => $workplace,
                'alert'=>true,
                'message'=>'workplacehasbeenchanged'
              ]);
            }
            else{
                return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
      
    

    }

     public function create(Request $request, $id)
    {
    
    

            $company=Company::findorFail($id);

            if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {
              $value=$request['values'];
              $workplace=WorkPlace::create(['name'=>$value['name'],'company_id'=>$id]);
            
           
              return response()->json([
                'success' => true,
                'data' => $workplace,
                'message'=>'workplacehasbeencreated',
                'alert'=>true
              ]);
            }
            else{
                return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
      


    

    }

    public function remove($id)
    {
   
       
           $workplace=WorkPlace::findorFail($id);

            $company=Company::findorFail($workplace->company_id);

              if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {

              $user=User::where('work_place_id',$id)->exists();

              if($user)
              { 
                
                $message='userhasworkplace';
              }
              else{
                $workplace->delete();
                
                $message='workplacehasbeenremoved';
              }
           
              return response()->json([
                'success' => true,
                'message' =>  $message,
                'alert'=>true
              ]);
            }
            else{
              return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
   
    }
}
