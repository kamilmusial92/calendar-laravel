<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\{Event,CalendarEvent,Notification, Company, WorkPlace, CompanyInvoices, IncorrectLogin, Payu, CompanyPayment};
use Illuminate\Support\Facades\Hash;
use App\Events\CreateUserEvent;
use Illuminate\Support\Str;
use Auth;

class ApiController extends Controller
{
    public function login(Request $request)
    {
        $searchincorrectlogins=IncorrectLogin::where('IP',Payu::getUserIP())->first();
          
        if($searchincorrectlogins && $searchincorrectlogins->count>=5)
        {   
            $now = date("Y-m-d H:i:s");
           $datediff = strtotime($now) - strtotime($searchincorrectlogins->last_login);
           $datediff=round($datediff / (60 ));
          if($datediff<10)
          {
             return response()->json([
                    'success' => false,
                    'message' => 'countincorrectlogins'
                   
                ], 401);
          }
          else{
            $searchincorrectlogins->update(['count'=>0]);
          }
        }
       

        if (Auth::attempt(['email' => request('username'), 'password' => request('password')])) {
            $user = Auth::user();
            $company=Company::with(['access'=>function($q) {
                
                  $q->where('status','COMPLETED')->orderBy('end','DESC')->first();
          
            }])->findorFail($user->company_id);
            if($company->access[0]->id!=1 && Auth::user()->hasRole(['User']) && $company->access[0]->end<date("Y-m-d H:i:s") && $user->active==1)
            {
              $user->update(['active'=>0]);
            }
            if($user->active==0 or $user->company->active==0)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Theaccountisinactive',
                ], 401);
            }
            elseif($user->verified==0)
            {
                 return response()->json([
                    'success' => false,
                    'message' => 'Theaccountisnotverified',
                ], 401);
            }
            elseif($user->active==1 && $user->verified==1)
            {
                if($searchincorrectlogins)
                {
                  $searchincorrectlogins->delete();
                }

              $success['token'] = $user->createToken('appToken')->accessToken;
              //After successfull authentication, notice how I return json parameters
              return response()->json([
                'success' => true,
                'token' => $success,
                'user' => $user
              ]);
            }
        } else {
       //if authentication is unsuccessfull, notice how I return json parameters
           if($searchincorrectlogins)
          {     
            if($searchincorrectlogins->count<5)
            {
                $searchincorrectlogins->update(['count'=>$searchincorrectlogins->count+1]);

                if($searchincorrectlogins->count>=5)
                {
                  return response()->json([
                      'success' => false,
                      'message' => 'countincorrectlogins',
                  ], 401);
                }
            }

          }
          else{
              IncorrectLogin::create(['IP'=>Payu::getUserIP(),'last_login'=>date('Y-m-d H:i:s'),'count'=>1]);
          }

          return response()->json([
            'success' => false,
            'message' => 'InvalidEmailorPassword',
        ], 401);
        }
    }

    public function activation($token)
    {
            
           $user=User::where('remember_token',$token)->first();
            if($user && $user->verified==0)
            {
              $user->update(['verified'=>1]);

               return response()->json([
                'success' => true,
                'message' => 'activated',
                'alert'=>true,
              ]);
            }
            else{
               return response()->json([
                'success' => false,
                'message' => 'tokeninvalid'
                
              ]);
            }
         
    }

     public function register(Request $request)
    {
         

             $value=$request['values'];
             $token=Str::random(20);

             $searchcompany=Company::where('name',$value['company'])->exists();
             if($searchcompany)
             {
                return response()->json([
                  'success' => false,
                  
                  'message'=>'companynamealreadyexists'
                ]);
             }
             $searchuser=User::where('email',$value['email'])->exists();
               if($searchuser)
             {
                return response()->json([
                  'success' => false,
                  
                  'message'=>'useremailalreadyexists'
                ]);
             }

              $company=Company::create(['name'=>$value['company'],'active'=>1]);
             $inv=CompanyInvoices::create(['status'=>'COMPLETED','subscription_id'=>1,'company_id'=>$company->id,'access_id'=>1,'start'=>date('Y-m-d H:i'),'end'=>date('Y-m-d H:i', strtotime(date('Y-m-d H:i'). ' + 7 days'))]);
              CompanyPayment::create(['invoice_id'=>$inv->id,'price'=>0,'renew'=>0]);
              $workplace=WorkPlace::create(['name'=>'CEO','company_id'=>$company->id]);
              $user=User::create(['name'=>$value['surname'],'email'=>$value['email'],'active'=>1,'company_id'=>$company->id,'avatar_path'=>env('APP_URL').'uploads/avatars/avatar_noname.jpg','work_place_id'=>$workplace->id,'password'=>Hash::make($value['password']),'remember_token'=>$token]);
              $user->roles()->attach(2);

              $link=env('APP_CALENDAR').'/activation/'.$token;

              if(env('APP_ENV')=='production')
              event(new CreateUserEvent($user, $value['password'],$link));
              

               return response()->json([
                  'success' => true,
                  'alert'=>true,
                  'message'=>'registersuccess'
                ]);
       
    }

       public function logout(Request $res)
    {
      if (Auth::user()) {
        $user = Auth::user()->token();
        $user->revoke();

        return response()->json([
          'success' => true,
          'message' => 'Logout successfully'
      ]);

      }
      else {
        return response()->json([
          'success' => false,
          'message' => 'Unable to Logout'
        ]);
      }
    }

   

 

}
