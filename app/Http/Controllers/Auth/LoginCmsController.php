<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Page\Shop\ShopCartCookies;
use Illuminate\Http\Request;

class LoginCmsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

public function logout(Request $request) {
  Auth::logout();
  return redirect('/cms/login');
}

    public function showLoginFormCms(){
        
       return view('auth/cms.login');
    }
 

    protected function redirectTo()
{
  
      
 
   
        if(Auth::User()->admin==1)
        {
        return '/cms/dashboard';
        }
        else{
         return '/cms';
        }
    
    
   
    
}

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
