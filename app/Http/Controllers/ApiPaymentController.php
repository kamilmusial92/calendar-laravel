<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\{Subscription,Payu,Paypal,User, CompanyInvoices, Company};
use Auth;
use GuzzleHttp\Client;

class ApiPaymentController extends Controller
{
      public function fetch()
    {
    
          	$list=Subscription::where('number_of_users','!=',5)->get();

          	return response()->json([
                  'success' => true,
                  'data' => $list
                 
            ]);
  
    }

  

       public function PayuNewOrder(Request $request)
    {
    	
      	$user = Auth::user()->token();

      	 
          	$user=User::findorFail(Auth::user()->id);
          	$token=$request->tok;
          	$subid=$request->id;
          	$price=$request->price;
          	$type=$request->type;
          	$lang=$request->lang;
			       return Payu::NewOrder($token,$subid,$price,$type,$user,$lang);

   
    }

      public function checkOrderStatus($id)
    {
    	
          	$user=User::findorFail(Auth::user()->id);
          	
          	$order=CompanyInvoices::with(['payment'])->findorFail($id);

          		$company=Company::with(['users'=>function($q) {
                 $q->whereHas('roles', function ($query) {
                	$query->where('name','Customer');
            	 });
            },'access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.name','access.payment','slack'])->find($order->company_id);
			     
            if(!$company)
            {
              return response()->json([
                'success' => true,
                'message' => 'accessdenied',
                  'data'=>[]
                 ]);
            }

            if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
          	{


          		if($order->payment[0]->payment_name=='Payu')
          		{


					     return Payu::checkOrder($order->payment[0]->orderId,$id);
				      }
              elseif($order->payment[0]->payment_name=='Paypal')
              {
                return Paypal::checkOrder($order->payment[0]->orderId,$id);
              }
			     }

			     else{
          		return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
          	}

    
    }

      public function checkPayments()
    {
    	
          	$user=User::findorFail(Auth::user()->id);
          	
          	

			     $order=Company::with(['access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.payment'])->find($user->company_id);
  
          		if($order->access[0]->payment && $order->access[0]->payment[0]->payment_name=='Payu')
          		{


					     return Payu::orderRecurring($order->access[0]->payment[0]->id,$user);
				      }
              elseif($order->access[0]->payment && $order->access[0]->payment[0]->payment_name=='Paypal')
              {
                return Paypal::orderRecurring($order->access[0]->payment[0]->id,$user);
              }
			

    }


       public function PaypalNewOrder(Request $request)
    {
      
            $user=User::findorFail(Auth::user()->id);
            $paypalsubid=$request->paypalsubid;
            $subid=$request->id;
            $price=$request->price;
            $type=$request->type;
            $lang='pl';

            return Paypal::NewOrder($paypalsubid,$subid,$price,$type,$user,$lang);

     
    }

      public function cancelOrder($id)
    {
      
            $user=User::findorFail(Auth::user()->id);

            $company=Company::with(['users'=>function($q) {
                 $q->whereHas('roles', function ($query) {
                  $query->where('name','Customer');
               });
            },'access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.name','access.payment','slack'])->find($user->company_id);
           
            if(!$company)
            {
              return response()->json([
                'success' => true,
                'message' => 'accessdenied',
                  'data'=>[]
                 ]);
            }

               if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {
            

              $order=CompanyInvoices::with(['payment'])->findorFail($id);
    
                if($order->payment && $order->payment[0]->payment_name=='Payu')
                {


                 return Payu::cancelOrder($order->payment[0]->id,$user);
                }
                elseif($order->payment && $order->payment[0]->payment_name=='Paypal')
                {
                  return Paypal::cancelOrder($order->payment[0]->id,$user);
                }
      
            }
              else{
              return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }

     
    }

         public function invoices($id)
    {
      
       
            $user=User::findorFail(Auth::user()->id);

              $company=Company::with(['users'=>function($q) {
                 $q->whereHas('roles', function ($query) {
                  $query->where('name','Customer');
               });
            },'access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.name','access.payment','slack'])->find($user->company_id);
           
            if(!$company)
            {
              return response()->json([
                'success' => true,
                'message' => 'accessdenied',
                  'data'=>[]
                 ]);
            }
            if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {
             $list=CompanyInvoices::with(['payment','name','subscription'])->where('company_id',$id)->orderBy("end","DESC")->get();

              return response()->json([
                    'success' => true,
                    'data' => $list
                   
              ]);
            }
            
           else{
              return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }

    }

}
