<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use App\Events\CreateUserEvent;

use App\User;
use App\{Event,CalendarEvent,Notification, WorkPlace, EventUsers, Company, QuestionnaireUsers, ContractUsers, BhpUsers, MedicalUsers, ResponsibilityUsers,CompanystuffUsers, DocumentUsers};
use Illuminate\Support\Facades\Hash;
use Auth;
use Storage;
use Illuminate\Support\Str;

class ApiUserController extends Controller
{
	public function info(Request $request)
    {
     
        
          
            
          	$user=User::with(['roles','workplace','workplace.permissions','company'])->findorFail(Auth::user()->id);
            $activeusers=User::where('company_id',Auth::user()->company_id)->where('active',1)->count();
          	
           
              return response()->json([
                'success' => true,
                'data' => $user,
                'activeusers'=>$activeusers
              ]);
     

    }

   public function userInfo(Request $request, $id)
    {
     
       
            $user=User::with(['workplace','roles','assignedevents','assignedevents.name','questionnaire','contracts','bhps','medicals','responsibility','companystuffs','documents'])->find($id);

            $permission=WorkPlace::findorFail(Auth::user()->work_place_id);
            
            if(!$user)
            {
              return response()->json([
                'success' => true,
                'message' => 'accessdenied',
                  'data'=>[]
                 ]);
            }
            
           if(Auth::user()->hasRole(['SuperAdmin']))
            {
                return response()->json([
                'success' => true,
                'data' => $user
                
              ]);
            }
            elseif(Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id)
            {
             
              
                 return response()->json([
                'success' => true,
                'data' => $user
                
              ]);
              
             
            }
              
            elseif($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id)
            {
                 return response()->json([
                'success' => true,
                'data' => $user
                
                ]);
            }
            else
            {

                 return response()->json([
                'success' => true,
                'message' => 'accessdenied'
                 ]);
              
            }
            
    

    }

    public function editInfo(Request $request, $id)
    {
    
        
        

            $value=$request['values'];

           if(is_numeric($id))
           {
            $edit=User::findorFail($id);
           }
           elseif(!empty(Auth::user()->id))
           {
            $edit=User::findorFail(Auth::user()->id);
           }
          	if(empty($value['workplace']))
            {
              $value['workplace']= $edit->work_place_id;
            }

          	$edit->update(['name'=>$value['surname'],'email'=>$value['email'],'work_place_id'=>$value['workplace'],'works_on_saturdays'=>$value['works_on_saturdays'],'works_on_sundays'=>$value['works_on_sundays']]);
           
           	
            $user=User::with(['workplace'])->findorFail($edit->id);

              return response()->json([
                'success' => true,
                'data' => $user,
                'message' =>'datahasbeenchanged'
              ]);
       


    }

      public function editAuthInfo(Request $request)
    {
     
        
            $value=$request['values'];

          
           if(!empty(Auth::user()->id))
           {
            $edit=User::findorFail(Auth::user()->id);
           }
            if(empty($value['workplace']))
            {
              $value['workplace']= $edit->work_place_id;
            }

            $edit->update(['name'=>$value['surname'],'email'=>$value['email'],'work_place_id'=>$value['workplace']]);
           
            
            $user=User::with(['roles','workplace','workplace.permissions','company'])->findorFail(Auth::user()->id);

              return response()->json([
                'success' => true,
                'data' => $user,
                'message' =>'dataauthhasbeenchanged'
              ]);
         


    }

     public function uploadAvatar(Request $request)
    {

   
              if($request->input('file'))
              {
              
              $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->input('file')));

               $path='avatars/avatar_'.Auth::user()->id.'_'.time().'.png';
               if(env('APP_ENV')=='local')
               {
                $storage=Storage::disk('public');
               }
               elseif(env('APP_ENV')=='production'){
                $storage=Storage::disk('public_html');
               }
                
                $edit=User::findorFail(Auth::user()->id);
                $deletename=substr($edit->avatar_path, strpos($edit->avatar_path, "avatars"));

                if($deletename!='avatars/avatar_noname.jpg')
                {
                  $storage->delete($deletename);
                }
                
                $storage->put($path, $data);

              
               $edit->update(['avatar_path'=>env('APP_URL').'uploads/'.$path]);

              $user=User::with(['roles','workplace','workplace.permissions','company'])->findorFail(Auth::user()->id);

                return response()->json([
                'success' => true,
                'message' => "uploadsuccess",
                 'data' => $user
                ]);
              }
              else{
                 return response()->json([
                'success' => false,
                'message' => 'file upload failed'
            ]);
              }
       
 

    }

      public function changePassword(Request $request)
    {
     
        
          
 			      $value=$request['values'];
           

         

          	$edit=User::findorFail(Auth::user()->id);
          	
          	 if(Hash::check($value['password'], $edit->password))
          	{
      				if($value['newpassword']==$value['newpasswordrepeat'])
      				{
      					$edit->update(['password'=>Hash::make($value['newpassword'])]);
      	  			 	return response()->json([
      	                'success' => true,
      	                
      	                'message' =>'passwordchanged'
      	             	]);
      				}
      				else{

      					return response()->json([
      		            'success' => false,
      		            'message' => 'passwordsdonotmatch',
      		          	], 401);
      				}
          	}
          	else{
          		return response()->json([
	            'success' => false,
	            'message' => 'incorrectoldpass',
	          ], 401);
          	}


    }

    public function add(Request $request)
    {
     
        
        
            $permission=WorkPlace::findorFail(Auth::user()->work_place_id);
        
             $user=User::findorFail(Auth::user()->id);
             if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id))
            {

             

              $value=$request['values'];

               $company=Company::with(['access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
              },'access.name','access.subscription'])->findorFail($user->company_id);

               $users=User::where('company_id',$user->company_id)->where('active',1)->count();

                if( $users>=$company->access[0]->subscription->number_of_users && $company->access[0]->name->name=='Trial')
              {
                return response()->json([
                'success' => true,
                
                'message'=>'trial.max3users',
               
              ]);
              }
              elseif($users>=$company->access[0]->subscription->number_of_users)
              {
                 return response()->json([
                'success' => true,
                
                'message'=>'subscription.maxusers',
               
              ]);
              }

              $pass=Str::random(8);
               $token=Str::random(20);

                  $searchuser=User::where('email',$value['email'])->exists();
               if($searchuser)
             {
                return response()->json([
                  'success' =>false,
                  
                  'message'=>'useremailalreadyexists'
                ]);
             }

              $user=User::create(['name'=>$value['surname'],'email'=>$value['email'],'company_id'=>Auth::user()->company_id,'avatar_path'=>'https://kmusial.pl/calendariv/uploads/avatars/avatar_noname.jpg','work_place_id'=>$value['workplace'],'password'=>Hash::make($pass),'active'=>1,'remember_token'=>$token]);
           
              $user->roles()->attach(3);

              $events=Event::where('company_id',$user->company_id)->get();

              foreach($events as $event)
              {
                EventUsers::create(['event_id'=>$event->id,'user_id'=>$user->id]);
              }
           
              $user->with('workplace');
              $user=User::with('workplace')->find($user->id);

              $link=env('APP_CALENDAR').'/activation/'.$token;

              if(env('APP_ENV')=='production')
                event(new CreateUserEvent($user, $pass,$link));

              return response()->json([
                'success' => true,
                'data' => $user,
                'alert'=>true,
                'message'=>'userhasbeenadded'
              ]);
            }
            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
        


    }

      public function users(Request $request)
    {
    
        
        
            if(Auth::user()->hasRole(['SuperAdmin','Customer']))
            {

              $users=User::with('workplace')->where('company_id',Auth::user()->company_id)->get();

          
           
                return response()->json([
                  'success' => true,
                  'data' => $users,
                  
                ]);
            }
            elseif(Auth::user()->hasRole(['User']))
            {

              $permission=WorkPlace::findorFail(Auth::user()->work_place_id);
              if($permission->hasPermission('Users'))
              {


              $users=User::with('workplace')->where('company_id',Auth::user()->company_id)->get();

          
           
                return response()->json([
                  'success' => true,
                  'data' => $users,
                  
                ]);
              }
              else{
                return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
              }
            }

            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
    

    }

      public function changestatus($id)
    {
    
        
        
            $permission=WorkPlace::findorFail(Auth::user()->work_place_id);

       
             $user=User::findorFail(Auth::user()->id);
             if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id))
            {

              $user=User::with('workplace')->find($id);

               $company=Company::with(['access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
              },'access.name','access.subscription'])->findorFail($user->company_id);

               $users=User::where('company_id',$user->company_id)->where('active',1)->count();

                if( $users>=$company->access[0]->subscription->number_of_users && $company->access[0]->name->name=='Trial' && $user->active==0)
              {
                return response()->json([
                'success' => true,
                'alert'=>true,
                'message'=>'trial.max3users',
               'data' => $user,
              ]);
              }
              elseif($users>=$company->access[0]->subscription->number_of_users  && $user->active==0)
              {
                 return response()->json([
                'success' => true,
                   'alert'=>true,
                'message'=>'subscription.maxusers',
               'data' => $user,
              ]);
              }
              else{

                $user->update(['active'=>!$user->active]);
                
             
                  return response()->json([
                    'success' => true,
                    'data' => $user,
                    'message' => 'changestatussuccess',
                  ]);
              }
            }
           

            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
          
  

    }

       public function delete($id)
    {
    
        
          
            $permission=WorkPlace::findorFail(Auth::user()->work_place_id);

        
             $user=User::findorFail(Auth::user()->id);
             $delete=User::find($id);

             if((Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id and $delete->company_id==$user->company_id) ) )
            {

             

              $delete->delete();
              
           
                return response()->json([
                  'success' => true,
                  'data' => [],
                  'alert'=>true,
                  'message' => 'deleteusersuccess',
                ]);
            }
           

            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
          
    

    }

         public function editQuestionnaire(Request $request,$id)
    {
     
         $user = Auth::user()->token();
           
           $permission=WorkPlace::findorFail(Auth::user()->work_place_id);
          
         
             if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id))
            {
              $find=QuestionnaireUsers::where('user_id',$id)->first();
            
              if($find)
              {
                $find->update($request['values']);
              }
              else{
                $find=QuestionnaireUsers::create(['user_id'=>$id]);
                 $find->update($request['values']);
              }

                $user=User::with(['workplace','roles','assignedevents','assignedevents.name','questionnaire'])->find($id);

              return response()->json([
                  'success' => true,
                  'data' => $find,
                  'message' => 'datahasbeenchanged',
                  'alert'=>true,
                ]);

            }
            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }


    

    }

          public function newContract(Request $request,$id)
    {
     
        
            $user = Auth::user()->token();
           $permission=WorkPlace::findorFail(Auth::user()->work_place_id);
          
        
             if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id))
            {

            
            
            
                $new=ContractUsers::create(['user_id'=>$id]);
                 $new->update($request['values']);
              

               

              return response()->json([
                  'success' => true,
                  'data' => $new,
                  'message' => 'contracthasbeendadded',
                  'alert'=>true,
                ]);

            }
            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }

     

    }

         public function newBHP(Request $request,$id)
    {
   
        
         $user = Auth::user()->token();
           $permission=WorkPlace::findorFail(Auth::user()->work_place_id);
          
       
             if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id))
            {

            
            
            
                $new=BhpUsers::create(['user_id'=>$id]);
                 $new->update($request['values']);
              

               

              return response()->json([
                  'success' => true,
                  'data' => $new,
                  'message' => 'bhphasbeendadded',
                  'alert'=>true,
                ]);

            }
            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }

    

    }

          public function newMedical(Request $request,$id)
    {
     
         $user = Auth::user()->token();
           
           $permission=WorkPlace::findorFail(Auth::user()->work_place_id);
          
       
             if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id))
            {

            
            
            
                $new=MedicalUsers::create(['user_id'=>$id]);
                 $new->update($request['values']);
              

               

              return response()->json([
                  'success' => true,
                  'data' => $new,
                  'message' => 'medicalhasbeendadded',
                  'alert'=>true,
                ]);

            }
            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }

    

    }


     public function editResponsibility(Request $request,$id)
    {
   
         $user = Auth::user()->token();
          
           $permission=WorkPlace::findorFail(Auth::user()->work_place_id);
          
        
             if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id))
            {

              $find=ResponsibilityUsers::where('user_id',$id)->first();
              if($find)
              {
                $find->update($request['values']);
              }
              else{
                $find=ResponsibilityUsers::create(['user_id'=>$id]);
                 $find->update($request['values']);
              }
            
              
               

               

              return response()->json([
                  'success' => true,
                  'data' => $find,
                  'message' => 'datahasbeenchanged',
                  'alert'=>true,
                ]);

            }
            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }

    

    }

          public function removeContract($id)
    {
    
         $user = Auth::user()->token();
          
           $permission=WorkPlace::findorFail(Auth::user()->work_place_id);
          
        
             if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id))
            {

            
            
            
                $new=ContractUsers::findorFail($id);
                 $new->delete($id);
              

               

              return response()->json([
                  'success' => true,
                  'data' => $new,
                  'message' => 'contracthasbeendremoved',
                  'alert'=>true,
                ]);

            }
            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }


    }

              public function removeCompanystuff($id)
    {
   
         $user = Auth::user()->token();
           
           $permission=WorkPlace::findorFail(Auth::user()->work_place_id);
          
       
             if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id))
            {

            
            
            
                $new=CompanystuffUsers::findorFail($id);
                 $new->delete($id);
              

               

              return response()->json([
                  'success' => true,
                  'data' => $new,
                  'message' => 'companystuffhasbeendremoved',
                  'alert'=>true,
                ]);

            }
            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }

   

    }

          public function newCompanystuff(Request $request,$id)
    {
   
         $user = Auth::user()->token();
          
           $permission=WorkPlace::findorFail(Auth::user()->work_place_id);
          
         
             if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id))
            {

            
            
            
                $new=CompanystuffUsers::create(['user_id'=>$id]);
                 $new->update($request['values']);
              

               

              return response()->json([
                  'success' => true,
                  'data' => $new,
                  'message' => 'companystuffhasbeendadded',
                  'alert'=>true,
                ]);

            }
            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }

     

    }

         public function newDocument(Request $request, $id)
    {

   

          $user = Auth::user()->token();
      

              
           $permission=WorkPlace::findorFail(Auth::user()->work_place_id);

              if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id))
            {
              
              if($_FILES['avatar'])
              {
            

                 $avatar_name = $_FILES["avatar"]["name"];
                  $avatar_tmp_name = $_FILES["avatar"]["tmp_name"];
                 $upload_name = preg_replace('/[^a-zA-Z0-9-_\.]/','', $avatar_name);

             
               
               if(env('APP_ENV')=='local')
               {
                $storage=Storage::disk('public');
               }
               elseif(env('APP_ENV')=='production'){
                $storage=Storage::disk('public_html');
               }
               $upload_name=$id."_".$upload_name;

                $document=DocumentUsers::create(['name'=>$upload_name,'path'=>env('APP_URL').'uploads/documents/'.$id.'/'.$upload_name,'user_id'=>$id]);

               $storage->put('documents/'.$id.'/'.$upload_name, file_get_contents($_FILES["avatar"]["tmp_name"]));

            


                return response()->json([
                'success' => true,
                'message' => "uploadfilesuccess",
                 'data' => $document,
                 'alert'=>true
                ]);
              }
              else{
                 return response()->json([
                'success' => false,
                'message' => 'file upload failed'
                ]);
              }
            }
            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
       

      

    }

             public function removeDocument($id)
    {
    
        
            $user = Auth::user()->token();
           $permission=WorkPlace::findorFail(Auth::user()->work_place_id);
          
        
             if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id) or ($permission->hasPermission('Users') and $user->company_id==Auth::user()->company_id))
            {

            
            
            
                $new=DocumentUsers::findorFail($id);

                if(env('APP_ENV')=='local')
               {
                $storage=Storage::disk('public');
               }
               elseif(env('APP_ENV')=='production'){
                $storage=Storage::disk('public_html');
               }
               $storage->delete('documents/'.$new->user_id.'/'.$new->name);
                 $new->delete($id);
              

               

              return response()->json([
                  'success' => true,
                  'data' => $new,
                  'message' => 'documenthasbeendremoved',
                  'alert'=>true,
                ]);

            }
            else{
               return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }


    }

      public function disableactiveusers(Request $request)
    {
    
    
        
            $user = Auth::user()->token();
        
            
            $accessusers=$request->accessusers;
            $countusers=$request->countusers;
            $count=$countusers-$accessusers;

            $activeusers=User::where('company_id',Auth::user()->company_id)->where('active',1)->whereHas('roles', function ($query) {
                  $query->where('name','User');
               })->get();

            for($i=0;$i<$count;$i++)
            {
              $us=User::where('company_id',Auth::user()->company_id)->where('active',1)->whereHas('roles', function ($query) {
                  $query->where('name','User');
               })->first();
              $us->update(['active'=>0]);
            }
            
            $activeusers=User::where('company_id',Auth::user()->company_id)->where('active',1)->count();

           
              return response()->json([
                'success' => true,
                'data' =>  $activeusers,
               
              ]);
        


    }

     
}
