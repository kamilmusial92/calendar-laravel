<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\{Permission, Company, Workplace};
use Auth;

class ApiPermissionController extends Controller
{
     public function list()
    {
    	
      	
          	$list=Permission::get();

          	return response()->json([
                  'success' => true,
                  'data' => $list
                 
            ]);
        
    
    }

         public function edit(Request $request,$companyid, $id)
    {
    	
      	$user = Auth::user()->token();

      	$company=Company::find($companyid);


      	
          	 if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {
            	$workplace=Workplace::where('company_id',$company->id)->find($id);

            	if($workplace)
            	{
					
					     $workplace->permissions()->wherePivot('work_place_id', $id)->detach();

	            	foreach($request['values'] as $value)
	            	{	
						
		             	 $workplace->permissions()->attach($value);
	
	            	}

	            return response()->json([
                  'success' => true,
                 	'alert'=>true,
                  'message'=>'permissionhasbeenchanged'
                ]);

	            }
            }
            else{
            	 return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }

    
    }
}
