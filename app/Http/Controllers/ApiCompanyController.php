<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\{Company,WorkPlace, User, SlackNotification, CompanyInvoices, CompanyPayment};
use Auth;
use Storage;
use App\Events\CreateUserEvent;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ApiCompanyController extends Controller
{
     public function info($id)
    {
    
        
            $user = Auth::user()->token();
        
           
          	$company=Company::with(['users'=>function($q) {
                 $q->whereHas('roles', function ($query) {
                	$query->where('name','Customer');
            	 });
            },'access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.name','access.payment','access.subscription','slack'])->find($id);
			     
            if(!$company)
            {
              return response()->json([
                'success' => true,
                'message' => 'accessdenied',
                  'data'=>[]
                 ]);
            }

          	if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
          	{


           
              return response()->json([
                'success' => true,
                'data' => $company,
                
              ]);
          	}
          	else{
          		return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
          	}


    }

     public function authinfo()
    {
    
        
            $user = Auth::user()->token();
        
           
            $company=Company::with(['users'=>function($q) {
                 $q->whereHas('roles', function ($query) {
                  $query->where('name','Customer');
               });
            },'access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.name','access.subscription','slack'])->find(Auth::user()->company_id);
           
    
              return response()->json([
                'success' => true,
                'data' => $company,
                
              ]);
  
    }

     public function add(Request $request)
    {
     
            $user = Auth::user()->token();

          	if(Auth::user()->hasRole(['SuperAdmin']))
          	{
          		$value=$request['values'];
               $pass=Str::random(8);
                $token=Str::random(20);

             $searchcompany=Company::where('name',$value['company'])->exists();
             if($searchcompany)
             {
                return response()->json([
                  'success' => false,
                  
                  'message'=>'companynamealreadyexists'
                ]);
             }
             $searchuser=User::where('email',$value['email'])->exists();
               if($searchuser)
             {
                return response()->json([
                  'success' =>false,
                  
                  'message'=>'useremailalreadyexists'
                ]);
             }

          		$company=Company::create(['name'=>$value['company'],'active'=>1]);
              $inv=CompanyInvoices::create(['status'=>'COMPLETED','subscription_id'=>1,'company_id'=>$company->id,'access_id'=>1,'start'=>date('Y-m-d H:i'),'end'=>date('Y-m-d H:i', strtotime(date('Y-m-d H:i'). ' + 7 days'))]);
              CompanyPayment::create(['invoice_id'=>$inv->id,'price'=>0,'renew'=>0]);

          		$workplace=WorkPlace::create(['name'=>'CEO','company_id'=>$company->id]);
          		$user=User::create(['name'=>$value['surname'],'email'=>$value['email'],'active'=>1,'company_id'=>$company->id,'work_place_id'=>$workplace->id,'password'=>Hash::make($pass),'remember_token'=>$token]);
           		$user->roles()->attach(2);

              $link=env('APP_CALENDAR').'/activation/'.$token;

              if(env('APP_ENV')=='production')
                event(new CreateUserEvent($user, $pass,$link));

              return response()->json([
                'success' => true,
                'data' => $company,
                'alert'=>true,
                'message'=>'customercreated'
              ]);
          	}
          	else{
          		return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
          	}

    }

     public function edit(Request $request, $id)
    {
     
        
            $user = Auth::user()->token();
   

            $company=Company::with(['users'=>function($q) {
                 $q->whereHas('roles', function ($query) {
                  $query->where('name','Customer');
               });
            },'access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.name'])->find($id);
           
            if(!$company)
            {
              return response()->json([
                'success' => true,
                'message' => 'accessdenied',
                  'data'=>[]
                 ]);
            }

            if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {

              $value=$request['values'];
             
              $company->update(['name'=>$value['company'],'url'=>$value['url']]);
             
              $user=User::findorFail($company->users[0]->id);

              $user->update(['email'=>$value['email']]);
             
               $company=Company::with(['users'=>function($q) {
                 $q->whereHas('roles', function ($query) {
                  $query->where('name','Customer');
               });
              },'access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.name','slack'])->find($id);
               

              return response()->json([
                'success' => true,
                'data' => $company,
               
                'message'=>'datahasbeenchanged',

              ]);
            }
            else{
              return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }

       


      
     

    }

       public function slackNotification(Request $request, $id)
    {
     
        
            $user = Auth::user()->token();
        
            
          
      

            $company=Company::with(['users'=>function($q) {
                 $q->whereHas('roles', function ($query) {
                  $query->where('name','Customer');
               });
            },'access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.name'])->find($id);
           
            if(!$company)
            {
              return response()->json([
                'success' => true,
                'message' => 'accessdenied',
                  'data'=>[]
                 ]);
            }

            if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {

              $value=$request['values'];
             
              $slack=SlackNotification::where('company_id',$id)->first();
              if($slack)
              {
                $slack->update(['channel_request'=>$value['channelrequest'],'channel_response'=>$value['channelresponse'],'webhook'=>$value['webhook']]);
              }
              else{
                 SlackNotification::create(['company_id'=>$id,'channel_request'=>$value['channelrequest'],'channel_response'=>$value['channelresponse'],'webhook'=>$value['webhook']]);
              }

             
               

              return response()->json([
                'success' => true,
                'data' => $company,
               
                'message'=>'datahasbeenchanged',

              ]);
            }
            else{
              return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }


    }

     public function changesidebar(Request $request, $id)
    {
     
        
            $user = Auth::user()->token();
        
            
          
            $company=Company::findorFail($id);

            if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {
              $value=$request['values'];

              

              $company->update(['sidebarcolor'=>$value['sidebarcolor']]);
              
              $authcompany=Company::findorFail(Auth::user()->company_id);

           
                return response()->json([
                  'success' => true,
                  'data' => $authcompany,
                  'companyinfo'=>$company,
                  'message'=>'sidebarhasbeenchanged'
                ]);

            }
            else{
              return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }


    }

     public function list()
    {
    
        
            $user = Auth::user()->token();
        
            
          	$companies=Company::with(['users'=>function($q) {
                 $q->whereHas('roles', function ($query) {
                	$query->where('name','Customer');
            	 });
            },'access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.name'])->get();
			

          	if(Auth::user()->hasRole(['SuperAdmin']))
          	{


           
              return response()->json([
                'success' => true,
                'data' => $companies,
                
              ]);
          	}
          	else{
          		return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
          	}

  
    }

       public function uploadLogotype(Request $request, $id)
    {

     

          $user = Auth::user()->token();
        
			     $company=Company::findorFail($id);

          	if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
          	{


              if($request->input('file'))
              {

              
                 $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->input('file')));

               $path='logotypes/logotype_'.$id.'_'.time().'.png';
                if(env('APP_ENV')=='local')
               {
                $storage=Storage::disk('public');
              
               }
               elseif(env('APP_ENV')=='production'){
                $storage=Storage::disk('public_html');
               
               }
               $deletename=substr($company->logo, strpos($company->logo, "logotypes"));
               $storage->delete($deletename);
                $storage->put($path, $data);
                
              
               $company->update(['logo'=>env('APP_URL').'uploads/'.$path]);

              

                return response()->json([
                'success' => true,
                'message' => "uploadlogotypesuccess",
                 'data' => $company
                ]);
              }
              else{
                 return response()->json([
                'success' => false,
                'message' => 'file upload failed'
            	]);
              }
            }
            else
            {
				        return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
  

    }

    
}
