<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\{Event,CalendarEvent,Notification, Company, WorkPlace, User, EventUsers, EventUserLogs};

class ApiEventsController extends Controller
{

    public function getEvents($id)
    {

             $company=Company::find($id);
            
            if(!$company)
            {
              return response()->json([
                'success' => true,
                'message' => 'accessdenied',
                  'data'=>[]
                 ]);
            }

          $events=Event::select('id','name','mark','borderColor','limit','company_id')->where('company_id',$company->id)->get();

          if ($events) {
          
            return response()->json([
              'success' => true,
              'data' => $events
            ]);

          }
          else {
            return response()->json([
              'success' => false,
              'message' => 'Error'
            ]);
          }
    }
         
     

    

     public function fetchEvents()
    {

          $events=Event::with(['assignedevents'=>function($q) {
            $q->where('user_id',Auth::user()->id);
            }])->where('company_id',Auth::user()->company_id)->get();
          if ($events) {
          
            return response()->json([
              'success' => true,
              'data' => $events
            ]);

          }
          else {
            return response()->json([
              'success' => false,
              'message' => 'Error'
            ]);
          }
       

    }

        public function fetchUserEvents($id)
    {

          $events=Event::with(['assignedevents'=>function($q) use ($id) {
            $q->where('user_id',$id);
            }])->where('company_id',Auth::user()->company_id)->get();
          if ($events) {
          
            return response()->json([
              'success' => true,
              'data' => $events
            ]);

          }
          else {
            return response()->json([
              'success' => false,
              'message' => 'Error'
            ]);
          }
       
   

    }

 	public function getEventsFromCalendar()
    {
      
        
        $user = Auth::user()->token();
        $events=CalendarEvent::with(['event','user.workplace','event.assignedevents','user'=>function($q) {
                $q->where('company_id',Auth::user()->company_id);
            }])->wherehas('user',function($q) {
                $q->where('company_id',Auth::user()->company_id);
            })->orderBy('updated_at','DESC')->get();

          $array=[];
      


         foreach($events as $event)
         {
          if($event->allDay==0)
          {
            $allday=false;
          }
          else{
            $allday=true;
          }

             foreach($event->event->assignedevents as $hour)
            {
              if($hour->user_id==$event->user_id)
              {
                $hours=$hour->hours;
              }
            }


            $array[]=array('id'=>$event->id,'title'=>$event->event->mark,'event_name'=>$event->event->name,'start'=>$event->start,'end'=>$event->end,'description'=>$event->description,'allDay'=>$allday,'borderColor'=>$event->event->borderColor,'status'=>$event->status,'surname'=>$event->user->name,'avatar'=>$event->user->avatar_path,'userid'=>$event->user->id,'updated'=>$event->updated_at,'limit'=>$event->event->limit,'hours'=>$hours,'workplace'=>$event->user->workplace->name);
         }
       

         
          return response()->json([
            'success' => true,
            'data' => $array,
            'user'=>$user
          ]);
      

    }

    public function getEventsUserFromCalendar($id)
    {
   
        
        $user = Auth::user()->token();
        $events=CalendarEvent::with(['event','user.workplace','event.assignedevents'=>function($q) use ($id) {
                $q->where('user_id',$id);
            },'user'=>function($q) {
                $q->where('company_id',Auth::user()->company_id);
            }])->wherehas('user',function($q) {
                $q->where('company_id',Auth::user()->company_id);
            })->where('user_id',$id)->orderBy('updated_at','DESC')->get();

          $array=[];
      


         foreach($events as $event)
         {
          if($event->allDay==0)
          {
            $allday=false;
          }
          else{
            $allday=true;
          }

          


            $array[]=array('id'=>$event->id,'title'=>$event->event->mark,'event_name'=>$event->event->name,'start'=>$event->start,'end'=>$event->end,'description'=>$event->description,'allDay'=>$allday,'borderColor'=>$event->event->borderColor,'status'=>$event->status,'surname'=>$event->user->name,'avatar'=>$event->user->avatar_path,'userid'=>$event->user->id,'updated'=>$event->updated_at,'limit'=>$event->event->limit,'hours'=>$event->event->assignedevents[0]->hours,'workplace'=>$event->user->workplace->name);
         }
       

         
          return response()->json([
            'success' => true,
            'data' => $array,
            'user'=>$user
          ]);
      
    



    }

    public function getToasts(Request $request)
    {
 
        $user = Auth::user()->token();
     

        $events=CalendarEvent::with(['notifications'=> function ($query) {
                 $query->where('status',2);
                },'event','user'=>function($q) {
                $q->where('company_id',Auth::user()->company_id);
            }])->
        wherehas('notifications',function ($q) {
             $q->where('status',2);
        })
        ->wherehas('user',function($q) {
                $q->where('company_id',Auth::user()->company_id);
            })
        ->where('user_id',Auth::user()->id)->where('status','>',1)->get();

          $array=[];

         foreach($events as $event)
         {
            $array[]=array('id'=>$event->id,'title'=>$event->event->mark,'start'=>$event->start,'end'=>$event->end,'description'=>$event->description,'allDay'=>$event->allDay,'borderColor'=>$event->event->borderColor,'status'=>$event->status,'surname'=>$event->user->name,'avatar'=>$event->user->avatar_path,'userid'=>$event->user->id);
         }

         
          return response()->json([
            'success' => true,
            'data' => $array,
            'user'=>$user
          ]);
   

    }



    public function addItemToCalendar(Request $request)
    {
 
        
            

            $value=$request['values'];
            $userid=$request['userid'];
            $title=explode(",", $value['title']);
            $start=date("Y-m-d H:i", strtotime($value['start']));
            $end=date("Y-m-d H:i", strtotime($value['end']));

            $events=Event::where('mark',$title[0])->where('company_id',Auth::user()->company_id)->first();
            


            if($value['allDay']==false)
              {
                $allday=0;
              }
              else{
                $allday=1;
              }

              if($userid)
              {
                $id=$userid;
                $status=2;

              }
              else{
                $id=Auth::user()->id;
                $status=1;
              }

              $company=Company::with(['access'=>function($query) {
                $query->where('status','COMPLETED')->orderBy('end','DESC');
              },'access.name'])->findorFail(Auth::user()->company_id);


              $eventsfromcalendar=CalendarEvent::where('user_id',$id)->where('status','!=',3)->count();

               if($company->access[0]->name->name=='Trial' && $eventsfromcalendar>=5)
              {
                return response()->json([
                'success' => true,
                
                'message'=>'trial.max5eventsfromcalendar',
               
              ]);
              }

      
            

              $event=CalendarEvent::create(['description'=>$value['content'],'status'=>$status,'start'=>$start,'end'=>$end,'allDay'=>$allday,'user_id'=>$id,'event_id'=>$events->id]);
               

            
                $eventuser=EventUsers::where('event_id',$event->event_id)->where('user_id',$event->user_id)->first();
                  $count=$eventuser->hours;

                 if($event->event->limit==1)
                {
                  

                  if($count>=$request['diff'])
                  {
                      $count=$count-$request['diff'];

                      $eventuser->update(['hours'=>$count]);
                  }
                  
                
                }

                 if($userid)
                {
                    if($event->event->limit==0)
                  {
                        $count=$count+$request['diff'];

                        $eventuser->update(['hours'=>$count]);
                  }
                }


               
              

              Notification::create(['calendar_event_id'=>$event->id,'status'=>$status]);



              $event=CalendarEvent::with(['event','event.assignedevents','user'])->findorFail($event->id);
          
              if($event->allDay==0)
              {
                $allday=false;
              } 
              else{
                $allday=true;
              }

                foreach($event->event->assignedevents as $hour)
            {
              if($hour->user_id==$event->user_id)
              {
                $hours=$hour->hours;
              }
            }
        
             $array=['id'=>$event->id,'title'=>$event->event->mark,'start'=>$event->start,'end'=>$event->end,'description'=>$event->description,'allDay'=>$allday,'borderColor'=>$event->event->borderColor,'status'=>$event->status,'surname'=>$event->user->name,'avatar'=>$event->user->avatar_path,'userid'=>$event->user->id,'updated'=>$event->updated_at,'limit'=>$event->event->limit,'hours'=>$hours];
             
              if($userid)
              {
                $eventslist=$this->fetchUserEvents($userid);
              }
              else{
                $eventslist=$this->fetchEvents();
                 
              }
              return response()->json([
                'success' => true,
                'eventsfromcalendar' => $array,
                'events'=>$eventslist
                
              ]);
   

    }

    public function changeItemStatus(Request $request)
    {
    
            $user = Auth::user()->token();
        
            
            $id=$request['id'];
            $status=$request['status'];
            $userid=$request['userid'];

              if($userid)
              {
                $userid=$userid;
               

              }
              else{
                $userid=Auth::user()->id;
              
              }

            $event=CalendarEvent::with(['event','event.assignedevents'=>function($w) use ($userid) {
              $w->where('user_id', $userid);
              },'user'])->findorFail($id);
            $value=$request['diff'];

            if($event->status==1)
            {
              
             
              $eventuser=EventUsers::where('event_id',$event->event_id)->where('user_id',$event->user_id)->first();
              
             
               $count=$eventuser->hours;

              if($status==2)
              {
               
                $message='eventaccepted';
                if($event->event->limit==0)
                {
                      $count=$count+$value;

                      $eventuser->update(['hours'=>$count]);
                }
              
              }
              elseif($status==3)
              {
                  $message='eventrejected';
                 if($event->event->limit==1)
                {
                      $count=$count+$value;

                      $eventuser->update(['hours'=>$count]);
                }
              }

               $event->update(['status'=> $status]);

              $notification=Notification::where('calendar_event_id',$id)->first();

              $notification->update(['status'=>2]);

            

    			  if($event->allDay==0)
    			     {
    			      $allday=false;
    			     }
    			     else{
    			       $allday=true;
    			     }

                 $event=CalendarEvent::with(['event','event.assignedevents','user'])->findorFail($id);

                   foreach($event->event->assignedevents as $hour)
                  {
                    if($hour->user_id==$event->user_id)
                    {
                      $hours=$hour->hours;
                    }
                  }

               $array=['id'=>$event->id,'title'=>$event->event->mark,'start'=>$event->start,'end'=>$event->end,'description'=>$event->description,'allDay'=>$allday,'borderColor'=>$event->event->borderColor,'status'=>$event->status,'surname'=>$event->user->name,'avatar'=>$event->user->avatar_path,'userid'=>$event->user->id,'updated'=>$event->updated_at,'limit'=>$event->event->limit,'hours'=>$hours];

                 if($userid)
              {
                $eventslist=$this->fetchUserEvents($userid);
              }
              else{
                $eventslist=$this->fetchEvents();
                 
              }

               return response()->json([
                'success' => true,
               'eventsfromcalendar' => $array,
                'events'=>$eventslist,

                 'alert'=>true,
                 'message'=>$message
              ]);
            }
           

             
           


    }

    public function updateToastStatus(Request $request)
    {
    
        
            $user = Auth::user()->token();
  
           
           $array=$request['data'];
           
            foreach($array as $data)
            {
               $event=Notification::where('calendar_event_id',$data['id'])->first();
           
               $event->update(['status'=>1]);
            }

              $events=CalendarEvent::with(['notifications'=> function ($query) {
                 $query->where('status',2);
                },'event','user'=>function($q) {
                $q->where('company_id',Auth::user()->company_id);
            }])->
        wherehas('notifications',function ($q) {
             $q->where('status',2);
        })->wherehas('user',function($q) {
                $q->where('company_id',Auth::user()->company_id);
            })->where('user_id',Auth::user()->id)->where('status','>',1)->get();

          $array=[];

         foreach($events as $event)
         {
            $array[]=array('id'=>$event->id,'title'=>$event->event->mark,'start'=>$event->start,'end'=>$event->end,'description'=>$event->description,'allDay'=>$event->allDay,'borderColor'=>$event->event->color,'status'=>$event->status,'surname'=>$event->user->name,'avatar'=>$event->user->avatar_path,'userid'=>$event->user->id);
         }
           
            
             
              return response()->json([
                'success' => true,
                 'data' => $array,
                
              ]);
       

    

    }


        public function edit(Request $request, $id)
    {
    
    
        
            $user = Auth::user()->token();
        
             $event=Event::findorFail($id);

            $company=Company::findorFail($event->company_id);

            if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {
              $value=$request['values'];
            

              $event->update(['name'=>$value['name'],'borderColor'=>$value['color'],'mark'=>$value['mark'],'limit'=>$value['limit']]);
            
           
              return response()->json([
                'success' => true,
                'data' => $event,
                'alert'=>true,
                'message'=>'eventhasbeenchanged'
              ]);
            }
            else{
                return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
        


    

    }

     public function create(Request $request, $id)
    {
    
        
            $user = Auth::user()->token();
       
            

            $company=Company::with(['access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.name'])->findorFail($id);

            if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {
              $value=$request['values'];
              $events=Event::where('company_id',$company->id)->count();
              if($company->access[0]->name->name=='Trial' && $events>=3)
              {
                return response()->json([
                'success' => true,
                
                'message'=>'trial.max3events',
               
              ]);
              }


              $event=Event::create(['name'=>$value['name'],'company_id'=>$id,'mark'=>$value['mark'],'borderColor'=>$value['color'],'limit'=>$value['limit']]);


             $users=User::where('company_id',$company->id)->get();

             foreach($users as $user)
             {
                EventUsers::create(['event_id'=>$event->id,'user_id'=>$user->id]);
             }
           
              return response()->json([
                'success' => true,
                'data' => $event,
                'message'=>'eventhasbeencreated',
                'alert'=>true
              ]);
            }
            else{
                return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
      


      

    }

      public function remove($id)
    {
     
        
            $user = Auth::user()->token();
        
            
           $event=Event::findorFail($id);

            $company=Company::findorFail($event->company_id);

              if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {

            

           
                $event->delete();
                
                $message='eventhasbeenremoved';
              
           
              return response()->json([
                'success' => true,
                'message' =>  $message,
                'alert'=>true
              ]);
            }
            else{
              return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
       


   

    }

         public function editAssigned(Request $request,$id)
    {
    
        
           
       
            $user=User::findorFail(Auth::user()->id);
         
            $company=Company::findorFail(Auth::user()->company_id);

              if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $company->id==Auth::user()->company_id))
            {

              foreach($request['values'] as $event)
              {
                $find=EventUsers::findorFail($event['id']);
                if(is_numeric($event['hours']))
                { 
                  $count=$event['hours']+$event['minutes']/60;
                
                    EventUserLogs::create(['user_id'=>$id,'event_id'=>$find->event_id,'admin_username'=>$user->name,'hours_before'=>$find->hours,'hours_after'=>$count]);
                    $find->update(['hours'=>$count,'active'=>$event['active']]);
                   
                  

                }
                else{
                  return response()->json([
                'success' => true,
                'message' =>  'hourisnotnumber',
                
              ]);
                }
                
             
              }

              return response()->json([
                'success' => true,
                'message' =>  'assignedeventshasbeenchanged',
                'alert'=>true
              ]);

            }
            else{
              return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
      


    

    }


       public function removeItemAccepted(Request $request,$id)
    {
    
        
            $user = Auth::user()->token();
        
            
           $event=CalendarEvent::with('event','user')->findorFail($id);
           $userid=$request['userid'];
           $user=User::find(Auth::user()->id);

              if(Auth::user()->hasRole(['SuperAdmin']) or (Auth::user()->hasRole(['Customer']) and $user->company_id==Auth::user()->company_id))
            {

            
                $eventuser=EventUsers::where('event_id',$event->event_id)->where('user_id',$event->user_id)->first();
                $count=$eventuser->hours;
                  if($event->event->limit==1)
                  {
                     $count=$count+$request['diffhours'];
                     
                  }
                  elseif($event->event->limit==0){
                     $count=$count-$request['diffhours'];
                  }


                $eventuser->update(['hours'=>$count]);

                $event->delete();

                 $message='eventhasbeenremoved';
              
                  if($userid)
              {
                $eventslist=$this->fetchUserEvents($userid);
              }
              else{
                $eventslist=$this->fetchEvents();
                 
              }
           
                return response()->json([
                  'success' => true,
                  'data'=>$id,
                  'events'=>$eventslist,
                  'message' =>  $message,
                  'alert'=>true
                ]);
               
                
                
               
            }
            else{
              return response()->json([
                  'success' => true,
                  'message' => 'accessdenied',
                  'data' =>[]
                ]);
            }
       


      
    

    }

    public function fetchEventsLogs($id)
    {
    
            $list=EventUserLogs::with('name')->where('user_id',$id)->orderBy('created_at','DESC')->get();

              return response()->json([
                  'success' => true,
                  'data'=>$list,
                
                ]);
       

  

    }

 

}
