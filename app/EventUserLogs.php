<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventUserLogs extends Model
{
      protected $fillable = [
        'user_id','event_id','hours_before','hours_after','admin_username'
    ];

        public function name()
    {
        return $this->belongsTo('App\Event', 'event_id');
    }
}
