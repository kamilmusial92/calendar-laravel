<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Events\CreateUserEvent;
use App\Events\CreateOrderEvent;
use App\Events\CancelOrderEvent;

use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use App\Listeners\SendCreateUserConfirmationEmail;
use App\Listeners\SendCreateOrderEmail;
use App\Listeners\SendCancelOrderEmail;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
          CreateUserEvent::class => [
            SendCreateUserConfirmationEmail::class
        ],
          CreateOrderEvent::class => [
            SendCreateOrderEmail::class
        ],
          CancelOrderEvent::class => [
            SendCancelOrderEmail::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
