<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncorrectLogin extends Model
{
     protected $fillable = [
        'IP','count','last_login'
    ];

}
