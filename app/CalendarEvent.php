<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarEvent extends Model
{
    protected $fillable = [
        'start', 'event_id', 'end','allDay','user_id','description','status'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function event(){
    	return $this->belongsTo('App\Event');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification','calendar_event_id');
    }
}
