<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlackNotification extends Model
{
     protected $fillable = [
        'channel_response','channel_request', 'company_id','webhook'
    ];
}
