<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
     protected $fillable = [
        'name', 'url', 'logo','active','sidebarcolor','postcode','city','address_building_number','address'
    ];

        public function users()
    {
        return $this->hasMany('App\User');
    }

     public function access()
    {
        return $this->hasMany('App\CompanyInvoices');
    }

      public function slack()
    {
        return $this->hasOne('App\SlackNotification');
    }
}
