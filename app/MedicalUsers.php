<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicalUsers extends Model
{
     protected $fillable = [
        'user_id', 'start','end'
    ];
}
