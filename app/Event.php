<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
     protected $fillable = [
        'name', 'mark', 'limit','company_id','borderColor'
    ];


     public function assignedevents()
    {
        return $this->hasMany('App\EventUsers');
    }
     
}
