<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use App\{CompanyPayment,CompanyInvoices, Subscription};
use App\Events\CreateOrderEvent;
use App\Events\CancelOrderEvent;

class Paypal extends Model
{	

	private static $env = 'sandbox';


    private static $username = 'AbSrA3zgGFLiLXnF7a19OpSAtVrkiX1C-iJrwdDDJ26KXH2eGODAxZJqBUNFwXfAdGyreA336c7BQsfE';

    private static $serviceUrl = '';

    private static $password = 'ELwis8bAOYc8gEiPBEdFrkWsv_OgSgGEMkj6S-9ylNR0Hq20Ts4wJEpozqfkE0VZojQ8gbLTy_d5X3x5';

     public static function setEnvironment($domain = 'paypal.com/v1')
    {

    	$environment=self::$env;
        $environment = strtolower($environment);
        $page="https://api";
        $domain = strtolower($domain);

   
        

        if ($environment == 'secure') {
            self::$serviceUrl = $page. '.' . $domain;
           
        } else if ($environment == 'sandbox') {
            self::$serviceUrl = $page.'.sandbox'. '.' . $domain;
           
        } 
    }

	  public static function OAuth()
     {
        $client = new \GuzzleHttp\Client();
        Paypal::setEnvironment();
        $path=self::$serviceUrl;
        $clientid=self::$username;
      	$secret=self::$password;
		$credentials = base64_encode($clientid.":".$secret);
        $body = "grant_type=client_credentials";
                $response = $client->post($path.'/oauth2/token',[
             'headers' => [
                'Content-Type'=>'application/x-www-form-urlencoded',
                'Authorization' => 'Basic ' . $credentials,
            ],

              'body' => $body
             
                
            ]);

         return $access_token=json_decode($response->getBody()->getContents())->access_token;
     }


      public static function NewOrder($paypalsubid,$subid,$price,$type,$user,$lang)
    {
    	  $access_token=Paypal::OAuth();
 
  			$company=Company::with(['users'=>function($q) {
                 $q->whereHas('roles', function ($query) {
                    $query->where('name','Customer');
                 });
            },'access'=>function($query) {
              $query->where('status','COMPLETED')->orderBy('end','DESC');
            },'access.name','access.payment','slack'])->find($user->company_id);

              $client = new \GuzzleHttp\Client();
        	 
              $path=self::$serviceUrl;

             $email=$user->email;
		        $firstname=substr($user->name, 0, strpos($user->name, " "));
		        $lastname=substr($user->name, strpos($user->name, " "));
		        $totalprice=floor(($price*100))/100;
		        $totalprice=intval($price*100);

                if($type=='monthly')
                {
                     $enddate=date("Y-m-d H:i", strtotime(date("Y-m-d H:i"). ' + 31 day + 2hours'));
                }
                elseif($type=='yearly')
                {
                     $enddate=date("Y-m-d H:i", strtotime(date("Y-m-d H:i"). ' + 366 day + 2hours'));
                }

                  $renewdate=date("Y-m-d",strtotime($enddate));
      			  $renewdate=date("Y-m-d H:i", strtotime($renewdate. ' + 12hours'));
      			  if($enddate<$renewdate)
      			  {
      			  	$enddate=date("Y-m-d H:i",strtotime($renewdate.' + 2hours'));
      			  }

                $subscription=Subscription::findorFail($subid);

                $invoice=CompanyInvoices::create(['subscription_id'=>$subid,'company_id'=>$user->company_id,'access_id'=>2,'start'=>date("Y-m-d H:i"), 'end'=>$enddate,'status'=>'APPROVAL_PENDING','renew_date'=>$renewdate]);
                $payment=CompanyPayment::create(['invoice_id'=>$invoice->id,'payment_name'=>"Paypal",'price'=>$totalprice/100,'type'=>$type,'lang'=>$lang,'orderId'=>$paypalsubid,'renew'=>1]);


                
		            $invoice=CompanyInvoices::findOrFail($invoice->id);
		           $link=env('APP_CALENDAR').'/settings/company/'.$user->company_id.'/invoices/'.$invoice->id;
		            $dateexpired=date("d-m-Y H:i",strtotime($invoice->renew_date));
					$orderid=$invoice->id;
		               
		               if(env('APP_ENV')=='production')
               		 event(new CreateOrderEvent($user,$dateexpired,$link,$orderid, 'New Order Subscription'));

				 return response()->json([
                  'success' => true,
                  'message'=>'completed',
                 'data'=>$link
                ]);
				
    }

     public static function checkSubscription($id,$invoiceid)
     {
        $access_token=Paypal::OAuth();
        $client = new \GuzzleHttp\Client();
       
        $path=self::$serviceUrl;

           $response = $client->get($path.'/billing/subscriptions/'.$id,[
		             'headers' => [
		                "Access-Control-Allow-Origin"=>"*",
		                "Access-Control-Allow-Methods"=> "GET",
		              
		                'Content-Type'=>'application/json',
		                "Authorization"=> "Bearer ".$access_token
		            ]

		              
		            
		            ]);
		      return $res=json_decode($response->getBody()->getContents());
	}

     public static function checkOrder($id,$invoiceid)
     {
       $res=Paypal::checkSubscription($id,$invoiceid);
           
           $order=CompanyInvoices::with(['payment','name'])->findorFail($invoiceid);
            $payment=CompanyPayment::where('invoice_id',$invoiceid)->first();
           
           	if($res->status=='ACTIVE')
           	{
           		$status='COMPLETED';
           	}
           	else{
           		$status=$order->status;
           	}
           

      

            $order->update(['status'=>$status]);

           

            return response()->json([
                  'success' => true,
                 'data'=>$order
                ]);
     }

        public static function cancelOrder($id,$user)
     {
         $payment=CompanyPayment::findorFail($id);
         $access_token=Paypal::OAuth();

        
         $payment->update(['token'=>'','renew'=>0]);
        
         $invoice=CompanyInvoices::with(['payment','name'])->findOrFail($payment->invoice_id);
 		$dateexpired=date("d-m-Y H:i",strtotime($invoice->renew_date));
         $orderid=$invoice->id;
        $link=env('APP_CALENDAR').'/settings/company/'.$user->company_id.'/invoices/'.$invoice->id;
         if(env('APP_ENV')=='production')
         event(new CancelOrderEvent($user,$dateexpired,$link,$orderid, 'Subscription canceled'));

          

         $client = new \GuzzleHttp\Client();
           
            $path=self::$serviceUrl;

 			$body= array(
                "reason"=> "Not satisfied with the service"
            );

           $response = $client->post($path.'/billing/subscriptions/'.$payment->orderId.'/cancel',[
                 'headers' => [
                    "Access-Control-Allow-Origin"=>"*",
                    "Access-Control-Allow-Methods"=> "POST",
                  
                    'Content-Type'=>'application/json',
                    "Authorization"=> "Bearer ".$access_token
                ],

              'body' => json_encode($body)

                
                
                ]);
            

	        return response()->json([
	            'success' => true,
	            'alert'=>true,
	            'message'=>"subscriptionhasbeencanceled",
	            'data'=>$invoice
	            ]);
           

     }


     public static function orderRecurring($id,$user)
     {
        $payment=CompanyPayment::findorFail($id);
        $invoice=CompanyInvoices::findorFail($payment->invoice_id);
        $subscription=Subscription::findorFail($invoice->subscription_id);
        $now = date("Y-m-d H:i"); // or your date as well
        
        $datediff = strtotime($now) - strtotime($invoice->start);
        $datediff=round($datediff / (60 * 60 * 24));
       
        if(($datediff>=30 && $payment->type=='monthly') or ($datediff>=365 && $payment->type=='yearly'))
        {
       
           

            $totalprice=floor(($payment->price*100))/100;
            $totalprice=intval($payment->price*100);

                    if($payment->type=='monthly')
                    {
                         $enddate=date("Y-m-d H:i", strtotime(date("Y-m-d H:i"). ' + 31 day + 2hours'));
                    }
                    elseif($payment->type=='yearly')
                    {
                         $enddate=date("Y-m-d H:i", strtotime(date("Y-m-d H:i"). ' + 366 day + 2hours'));
                    }

                  $renewdate=date("Y-m-d",strtotime($enddate));
      			  $renewdate=date("Y-m-d H:i", strtotime($renewdate. ' + 12hours'));
      			  if($enddate<$renewdate)
      			  {
      			  	$enddate=date("Y-m-d H:i",strtotime($renewdate.' + 2hours'));
      			  }
                    $payment->update(['renew'=>0]);

                    $invoice=CompanyInvoices::create(['subscription_id'=>$subscription->id,'company_id'=>$user->company_id,'access_id'=>2,'start'=>date("Y-m-d H:i"), 'end'=>$enddate,'status'=>'APPROVAL_PENDING','renew_date'=>$renewdate]);
                    $newpayment=CompanyPayment::create(['invoice_id'=>$invoice->id,'payment_name'=>"Paypal",'price'=>$totalprice/100,'type'=>$payment->type,'lang'=>$payment->lang,'renew'=>1,'orderId'=>$payment->orderId]);

               $res=Paypal::checkSubscription($payment->orderId,$invoice->id);

                 

                   if($res->status=='ACTIVE')
                   {
                    $invoice->update(['status'=>'COMPLETED']);
                  
                    $invoice=CompanyInvoices::findOrFail($invoice->id);
                     $dateexpired=date("d-m-Y H:i",strtotime($invoice->renew_date));
                     $orderid=$invoice->id;
                    $link=env('APP_CALENDAR').'/settings/company/'.$user->company_id.'/invoices/'.$invoice->id;
                    if(env('APP_ENV')=='production')
                    event(new CreateOrderEvent($user,$dateexpired,$link,$orderid, 'Subscription renewal'));

                    return response()->json([
                      'success' => true,
                      'message'=>'completed',
                     'data'=>$link
                    ]);
                   }
                  
                   else
                   {
                    $invoice->update(['status'=>'CANCELED']);
                     return response()->json([
                      'success' => true,
                     'data'=>"error",
                     'message'=>'Canceled'
                    ]);
                   }
        }
     }

}
