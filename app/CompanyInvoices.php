<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyInvoices extends Model
{

	  protected $fillable = [
        'start', 'end', 'access_id','company_id','subscription_id','status','renew_date'
    ];

        public function name()
    {
        return $this->belongsTo('App\AccessVersion', 'access_id');
    }

        public function payment()
    {
        return $this->hasMany('App\CompanyPayment', 'invoice_id');
    }

         public function subscription()
    {
        return $this->belongsTo('App\Subscription', 'subscription_id');
    }
}
