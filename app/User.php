<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;
    
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','company_id','work_place_id','avatar_path','active','verified','remember_token','works_on_sundays','works_on_saturdays'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

     public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

      public function assignedevents()
    {
        return $this->hasMany('App\EventUsers');
    }


       public function workplace()
    {
        return $this->belongsTo('App\WorkPlace', 'work_place_id');
    }

      public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

       public function questionnaire()
    {
        return $this->HasMany('App\QuestionnaireUsers');
    }

       public function contracts()
    {
        return $this->HasMany('App\ContractUsers');
    }
       public function bhps()
    {
        return $this->HasMany('App\BhpUsers');
    }

       public function medicals()
    {
        return $this->HasMany('App\MedicalUsers');
    }

      public function responsibility()
    {
        return $this->HasMany('App\ResponsibilityUsers');
    }
   
      public function companystuffs()
    {
        return $this->HasMany('App\CompanystuffUsers');
    }

       public function documents()
    {
        return $this->HasMany('App\DocumentUsers');
    }

      public function hasRole(array $roles)
    {
        foreach($roles as $role)
        {
           
                if($this->roles()->where('name',$role)->exists())
                {
                    return true;
                }


                
            
        }
        
    }

  


}
