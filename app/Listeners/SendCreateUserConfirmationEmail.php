<?php

namespace App\Listeners;

use App\Events\CreateUserEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\CreateUserEmail;

class SendCreateUserConfirmationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateUserEvent  $event
     * @return void
     */
    public function handle(CreateUserEvent $event)
    {
         \Mail::to($event->user->email)->send(
            new CreateUserEmail($event->user, $event->password, $event->link)
        );
    }
}
