<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\CancelOrder;
use App\Events\CancelOrderEvent;

class SendCancelOrderEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
         \Mail::to($event->user->email)->send(
            new CancelOrder($event->user, $event->expired, $event->link, $event->orderid, $event->subject)
        );
    }
}
