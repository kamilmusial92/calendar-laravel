<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Mail\CreateOrder;
use App\Events\CreateOrderEvent;
class SendCreateOrderEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        \Mail::to($event->user->email)->send(
            new CreateOrder($event->user, $event->expired, $event->link, $event->orderid, $event->subject)
        );
    }
}
