<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPayment extends Model
{
      protected $fillable = [
        'invoice_id', 'payment_name', 'token','orderId','price','type','lang','renew'
    ];

     protected $hidden = [
        'token', 'orderId',
    ];
}
