<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventUsers extends Model
{
     protected $fillable = [
        'user_id','event_id','hours','active'
    ];

      public function name()
    {
        return $this->belongsTo('App\Event', 'event_id');
    }

   
}
