<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanystuffUsers extends Model
{
      protected $fillable = [
        'user_id', 'name'
    ];
}
