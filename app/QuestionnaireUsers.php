<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireUsers extends Model
{
     protected $fillable = [
        'first_name','second_name','name','user_id','family_name','PESEL','fathers_name','mothers_name','NIP',
        'tax_office','citizenship','number_of_proof','date_of_birth','place_of_birth','education','experience',
        'phone_number','country','province','county','community','address','house_number','post_code','city','disability','student','insurance','company','bank_name','account_number'
    ];
}
