<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractUsers extends Model
{
      protected $fillable = [
        'user_id', 'company_name', 'type_time','contract_name','start','end'
    ];
}
