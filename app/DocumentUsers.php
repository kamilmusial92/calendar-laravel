<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentUsers extends Model
{
     protected $fillable = [
        'user_id', 'path','name'
    ];
}
