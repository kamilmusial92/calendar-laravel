<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkPlacePermission extends Model
{
     protected $fillable = [
        'work_place_id','permission_id'
    ];
}
