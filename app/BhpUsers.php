<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BhpUsers extends Model
{
     protected $fillable = [
        'user_id', 'start','end'
    ];
}
