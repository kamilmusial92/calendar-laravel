<?php

use Illuminate\Database\Seeder;

class SlackNotificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('slack_notifications')->insert([
        	'channel_request'=>'#testy-kalendarza',
        	'channel_response'=>'#testy-kalendarza',
			'company_id'=>1,
			'webhook'=>'https://hooks.slack.com/services/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'
        	]);
    }
}
