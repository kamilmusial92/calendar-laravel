<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(AccessVersionTableSeeder::class);
         $this->call(CompaniesTableSeeder::class);
         $this->call(WorkPlaceTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(RoleTableSeeder::class);
         $this->call(RoleUserTableSeeder::class);
 		 $this->call(EventsTableSeeder::class);
         $this->call(EventUsersTableSeeder::class);
         $this->call(PermissionsTableSeeder::class);
         $this->call(PermissionWorkPlaceTableSeeder::class);
         $this->call(SlackNotificationsTableSeeder::class);
          $this->call(SubscriptionsTableSeeder::class);
         $this->call(CompanyInvoicesTableSeeder::class);
        $this->call(CompanyPaymentsTableSeeder::class);

    }
}
