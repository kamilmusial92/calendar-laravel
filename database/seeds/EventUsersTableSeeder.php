<?php

use Illuminate\Database\Seeder;

class EventUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('event_users')->insert([
        	
            'event_id'=>1,
			'user_id'=>1,
            'hours'=>0,
            'active'=>1
        	]);

         DB::table('event_users')->insert([
        	
            'event_id'=>2,
			'user_id'=>1,
            'hours'=>4,
            'active'=>1
        	]);

          DB::table('event_users')->insert([
        	
            'event_id'=>2,
			'user_id'=>2,
            'hours'=>0,
            'active'=>1
        	]);

           DB::table('event_users')->insert([
        	
            'event_id'=>1,
			'user_id'=>2,
            'hours'=>0,
            'active'=>1
        	]);
    }
}
