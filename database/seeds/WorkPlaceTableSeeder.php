<?php

use Illuminate\Database\Seeder;

class WorkPlaceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* DB::table('work_places')->insert([
        	'name'=>'Księgowy',
			'company_id'=>2
        	]);*/

          DB::table('work_places')->insert([
        	'name'=>'Programista',
			'company_id'=>1
        	]);

           DB::table('work_places')->insert([
        	'name'=>'Prezes',
			'company_id'=>1
        	]);
    }
}
