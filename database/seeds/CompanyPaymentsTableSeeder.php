<?php

use Illuminate\Database\Seeder;

class CompanyPaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('company_payments')->insert([
        	'invoice_id'=>1,
        	'price'=>0,
        	'renew'=>0
        	]);
    }
}
