<?php

use Illuminate\Database\Seeder;

class AccessVersionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('access_versions')->insert([
        	'name'=>'Trial'
           
          ]);

          DB::table('access_versions')->insert([
        	'name'=>'Subscription'
           
          ]);

          DB::table('access_versions')->insert([
        	'name'=>'Full'
           
          ]);
    }
}
