<?php

use Illuminate\Database\Seeder;

class SubscriptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('subscriptions')->insert([
            'name'=>'Trial',
            'number_of_users'=>5,
            'month_price'=>0,
            'year_price'=>0,
            'color'=>'#CDCDCD'
            ]);

         DB::table('subscriptions')->insert([
        	'name'=>'Min',
			'number_of_users'=>10,
			'month_price'=>80,
			'year_price'=>60,
            'color'=>'#CDCDCD',
            'paypal_plan_yearly'=>"P-2B079740DG355654CL6DLG3Y",
            'paypal_plan_monthly'=>"P-66R87189MT296725WL6DLF6Q"
        	]);

          DB::table('subscriptions')->insert([
        	'name'=>'Medium',
			'number_of_users'=>50,
			'month_price'=>160,
			'year_price'=>120,
             'color'=>'#6DE23F',
            'paypal_plan_yearly'=>"P-3NT01261ER558521FL6DLHJI",
            'paypal_plan_monthly'=>"P-05J707868W899850XL6DLHZA"
        	]);

          DB::table('subscriptions')->insert([
        	'name'=>'Max',
			'number_of_users'=>250,
			'month_price'=>260,
			'year_price'=>240,
             'color'=>'#EE23CC',
            'paypal_plan_yearly'=>"P-6PS48558BJ963793HL6DLHOQ",
            'paypal_plan_monthly'=>"P-4RV37385E8768434BL6DLHUY"
             
        	]);
    }
}
