<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('companies')->insert([
        	'name'=>'Interactivevision',
            'active'=>1,
            'address'=>'Przemysłowa',
            'address_building_number'=>"14b",
            'city'=>"Rzeszów",
            "postcode"=>"35-105"
           // 'access_version_id'=>3
        	]);

         /*  DB::table('companies')->insert([
            'name'=>'Test company',
            'active'=>1,
            'access_version_id'=>2
            ]);*/
    }
}
