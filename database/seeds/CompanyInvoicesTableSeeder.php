<?php

use Illuminate\Database\Seeder;

class CompanyInvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('company_invoices')->insert([
        	'company_id'=>1,
        	'access_id'=>3,
        	'start'=>date('Y-m-d H:i'),
            'subscription_id'=>4,
            'status'=>'COMPLETED'
        	]);
    }
}
