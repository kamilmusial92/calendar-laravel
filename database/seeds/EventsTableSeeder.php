<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('events')->insert([
        	'name'=>'Choroba',
            'company_id'=>1,
			'limit'=>0,
            'mark'=>'C',
            'borderColor'=>'#0b52bc'
        	]);

           DB::table('events')->insert([
        	'name'=>'Urlop Wypoczynkowy',
            'company_id'=>1,
            'limit'=>1,
            'mark'=>'UW',
            'borderColor'=>'#39ad1a'
            
        	]);
    }
}
