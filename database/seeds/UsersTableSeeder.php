<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('users')->insert([
        	'name'=>'Kamil Musiał',
			    'email'=>'k.musial@interactivevision.pl',
          'company_id'=>1,
          'work_place_id'=>1,
			    'password'=>bcrypt('Intervi700'),
          'avatar_path'=>'https://kmusial.pl/calendariv/uploads/avatars/avatar_1.png',
          'active'=>1,
          'verified'=>1
        	]);
          
          DB::table('users')->insert([
          'name'=>'Mariusz Trzeciak',
          'email'=>'kontakt@interactivevision.pl',
          'company_id'=>1,
          'work_place_id'=>2,
          'password'=>bcrypt('mariusz'),
          'avatar_path'=>'https://kmusial.pl/calendariv/uploads/avatars/avatar_2.jpg',
          'active'=>1,
          'verified'=>1
          ]);

         /*  DB::table('users')->insert([
          'name'=>'Test user',
          'email'=>'test@test.pl',
          'company_id'=>2,
           'work_place_id'=>1,
          'password'=>bcrypt('Intervi700'),
          'avatar_path'=>'',
           'active'=>1
          ]);

            DB::table('users')->insert([
          'name'=>'BB',
          'email'=>'b@b.pl',
          'company_id'=>2,
           'work_place_id'=>1,
          'password'=>bcrypt('Intervi700'),
          'avatar_path'=>'',
           'active'=>1
          ]);*/
    }
}
