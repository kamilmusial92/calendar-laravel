<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnaireUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaire_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('first_name')->nullable();
            $table->string('second_name')->nullable();
            $table->string('name')->nullable();
            $table->string('family_name')->nullable();
            $table->string('PESEL')->nullable();
            $table->string('fathers_name')->nullable();
            $table->string('mothers_name')->nullable();
            $table->string('NIP')->nullable();
            $table->string('tax_office')->nullable();
            $table->string('citizenship')->nullable();
            $table->string('number_of_proof')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->string('education')->nullable();
            $table->string('experience')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('country')->nullable();
         
            $table->string('province')->nullable();
            $table->string('county')->nullable();
            $table->string('community')->nullable();
            $table->string('address')->nullable();
            $table->string('house_number')->nullable();
            $table->string('post_code')->nullable();
            $table->string('city')->nullable();
            $table->string('disability')->default('brak');
            $table->boolean('student')->default(0);
            $table->string('insurance')->nullable();
            $table->boolean('company')->default(0);
            $table->string('bank_name')->nullable();
            $table->string('account_number')->nullable();

          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_questionnaire_users');
    }
}
