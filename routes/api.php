<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['prefix' => 'v1'], function () {
    Route::post('/login', 'ApiController@login');
    Route::get('/activation/{activation}', 'ApiController@activation');
    Route::post('/register', 'ApiController@register');
    Route::get('/logout', 'ApiController@logout')->middleware('auth:api');
   
    Route::prefix('events')->middleware('auth:api')->group(function () {

        Route::post('/additem', 'ApiEventsController@addItemToCalendar');
         Route::post('/changeitemstatus', 'ApiEventsController@changeItemStatus');
        Route::post('/updatetoaststatus', 'ApiEventsController@updateToastStatus');
        Route::get('/geteventsfromcalendar', 'ApiEventsController@getEventsFromCalendar');
        Route::get('/gettoasts', 'ApiEventsController@getToasts');
        Route::get('/fetchevents', 'ApiEventsController@fetchEvents');
        Route::get('/fetchuserevents/{id}', 'ApiEventsController@fetchUserEvents');
        Route::get('/geteventsuserfromcalendar/{id}', 'ApiEventsController@getEventsUserFromCalendar');
        Route::get('/getevents/{id}', 'ApiEventsController@getEvents');
        Route::get('/remove/{id}', 'ApiEventsController@remove');
        Route::post('/edit/{id}', 'ApiEventsController@edit');
        Route::post('/create/{id}', 'ApiEventsController@create');
        Route::post('/editassigned/{id}', 'ApiEventsController@editAssigned');
        Route::post('/removeitemaccepted/{id}', 'ApiEventsController@removeItemAccepted');
        Route::get('/fetcheventslogs/{id}', 'ApiEventsController@fetchEventsLogs');


    });
    Route::prefix('user')->middleware('auth:api')->group(function () {

    	Route::get('/info', 'ApiUserController@info');
        Route::get('/userInfo/{id}', 'ApiUserController@userInfo');
        Route::get('/delete/{id}', 'ApiUserController@delete');
        Route::get('/changestatus/{id}', 'ApiUserController@changestatus');
    	Route::post('/editinfo/{id}', 'ApiUserController@editInfo');
        Route::post('/editauthinfo', 'ApiUserController@editAuthInfo');
        Route::post('/uploadavatar', 'ApiUserController@uploadAvatar');
        Route::post('/add', 'ApiUserController@add');
		Route::post('/changepassword', 'ApiUserController@changePassword');
        Route::get('/users', 'ApiUserController@users');
        Route::post('/disableactiveusers', 'ApiUserController@disableActiveUsers');

        Route::post('/editquestionnaire/{id}', 'ApiUserController@editQuestionnaire');
        Route::post('/newcontract/{id}', 'ApiUserController@newContract');
        Route::get('/removecontract/{id}', 'ApiUserController@removeContract');
        Route::post('/newbhp/{id}', 'ApiUserController@newBHP');
        Route::post('/newmedical/{id}', 'ApiUserController@newMedical');
        Route::post('/editresponsibility/{id}', 'ApiUserController@editResponsibility');
        Route::post('/newcompanystuff/{id}', 'ApiUserController@newCompanystuff');
        Route::get('/removecompanystuff/{id}', 'ApiUserController@removeCompanystuff');
        Route::post('/newdocument/{id}', 'ApiUserController@newDocument');
        Route::get('/removedocument/{id}', 'ApiUserController@removeDocument');
      



    });

    Route::prefix('subscription')->middleware('auth:api')->group(function () {
       
        Route::get('/fetch', 'ApiPaymentController@fetch');
        Route::post('/payu/new_order', 'ApiPaymentController@PayuNewOrder');
        Route::get('/checkorderstatus/{id}', 'ApiPaymentController@checkOrderStatus');
        Route::get('/checkpayments', 'ApiPaymentController@checkPayments');
        Route::get('/cancelorder/{id}', 'ApiPaymentController@cancelOrder');

        
        Route::post('/paypal/new_order', 'ApiPaymentController@PaypalNewOrder');
        Route::get('/fetchinvoices/{id}', 'ApiPaymentController@invoices');
    });


    Route::prefix('workplace')->middleware('auth:api')->group(function () {
        Route::get('/authinfo', 'ApiWorkPlaceController@authinfo');
        Route::get('/info/{id}', 'ApiWorkPlaceController@info');
        Route::get('/remove/{id}', 'ApiWorkPlaceController@remove');
        Route::post('/edit/{id}', 'ApiWorkPlaceController@edit');
        Route::post('/create/{id}', 'ApiWorkPlaceController@create');
    });

    Route::prefix('company')->middleware('auth:api')->group(function () {
        Route::get('/info/{id}', 'ApiCompanyController@info');
        Route::get('/authinfo', 'ApiCompanyController@authinfo');

        Route::post('/add', 'ApiCompanyController@add');
        Route::post('/edit/{id}', 'ApiCompanyController@edit');
        Route::post('/slacknotification/{id}', 'ApiCompanyController@slackNotification');

        Route::post('/changesidebar/{id}', 'ApiCompanyController@changesidebar');
        Route::get('/list', 'ApiCompanyController@list');
        Route::post('/uploadlogotype/{id}', 'ApiCompanyController@uploadLogotype');
        
    });

    Route::prefix('permissions')->middleware('auth:api')->group(function () {
         Route::get('/list', 'ApiPermissionController@list');
         Route::post('/edit/{companyid}/{id}', 'ApiPermissionController@edit');
    });
});
